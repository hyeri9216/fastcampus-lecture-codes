# 04. Firebase 정보와 사진 함께 올리기

## `Firestore`에 새 정보 올리기
`웹 > 데이터 읽기 및 쓰기` 참조

<br>

> `photos` 배열 비우기  
```javascript
var photos = [];
```

<br>

1. 사진정보 올리는 함수 만들기
```javascript
function uploadPhotoInfo (url) {
  // ...
}
```
> `uploadFile` 함수에서 파일 업로드하고 이미지 주소 받아 실행
```javascript
// ...
uploadPhotoInfo(url);
```

<br>

2. 사진정보 데이터베이스에 저장

> 저장할 사진정보 객체로 생성
```javascript
function uploadPhotoInfo (url) {
  var photoInfo = {
    idx: Date.now(),
    url: url,
    user_id: my_info.id,
    user_name: my_info.user_name,
    description: document.querySelector("input.description").value,
    likes: Math.round(Math.random() * 10)
  }
}
```
> 파이어베이스에 저장하기
```javascript
function uploadPhotoInfo (url) {
  // ...
  db.collection("photos").doc(String(photoInfo.id)).set(photoInfo)
  .then(function () {
    console.log("Success!");
  })
  .catch(function (error) {
    console.error("Error!", error);
  });
}
```

<br>

3. 업로드한 사진정보들 받아오기.

> 함수 생성
```javascript
function loadPhotos () {
}
```
> `init` 함수에 추가
```javascript
function init() {
  loadMyInfo();
  loadPhotos();
}
```
> `photos` 컬렉션으로부터 사진 데이터들 받아오기
```javascript
function loadPhotos () {
  db.collection("photos").get().then(function (querySnapshot) {
    querySnapshot.forEach(function (doc) {
      console.log(doc.data())
    })
  });
}
```
> 받아온 정보들을 `photos` 배열에 넣고 사진 보여주기
```javascript
function loadPhotos () {
  db.collection("photos").get().then(function (querySnapshot) {
    var photosArray = []
    querySnapshot.forEach(function (doc) {
      photosArray.push(doc.data())
    })
    photos = photosArray;
    showPhotos();
  });
}
```
> `showPhotos` 함수 url 관련 부분 변경
```javascript
// ...
photoNode.querySelector(".photo").style.backgroundImage 
  = "url('" + photo.url + "')";
// ...
```
> 업로드 후 갤러리로 이동 후 다시 로드하도록 하기
```javascript
// ...
setMenu('gallery');
loadPhotos();
// ...
```

<br>

***

<br>

## 다음 강좌
* [05. Firebase 좋아요와 팔로우 기능 넣기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/05-firebase/05/README.md)