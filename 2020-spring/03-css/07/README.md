# 07. CSS Material Design

> <a href="https://material.io/design/" target="_blank">공식 소개페이지</a>  
> <a href="http://media.fastcampus.co.kr/insight/branding-google/" target="_blank">패스트캠퍼스의 머티리얼 디자인 기사</a>

<br>

## 탄생 배경
![스큐어모픽-플랫-머티리얼](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/03-css/07/screenshots/sk_fl_mt.jpg)   

> <a href="https://www.besuccess.com/report/90214/material-design/" target="_blank">이미지 출처</a>

<br>
<br>

***

<br>
<br>

![스큐어모픽](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/03-css/07/screenshots/skeuo.png)  

### 스큐어모피즘
> `ex` iOS7 이전
* 실제 사물처럼 질감, 광원까지 상세히 디자인  

<br>
<br>

***

<br>
<br>

![플랫](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/03-css/07/screenshots/flat.png)   


### 플랫 디자인
> `ex` iOS7부터
* 색종이를 이어붙인 듯 평평한 디자인
* 입체효과, 그라디언트, 세부장식 배제  

<br>
<br>

***

<br>
<br>

![머티리얼](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/03-css/07/screenshots/material.png)   


### 머티리얼 디자인
> 구글이 2014년 도입
* 플랫 디자인에 그림자로 깊이감 추가 (요소별 높낮이 표현)
* 역동적인 애니메이션 활용

<br>
<br>

***

<br>
<br>

## 머티리얼 디자인 도입 이유

### 1. 일관성 있는 UX
* 어떤 기기나 앱을 사용하든 익숙한 사용자 경험
* 어떤 컨텐츠든 담을 수 있는 디자인

<br>

### 2. 의도 명확화
* 어떤 요소든 용도와 작동방법을 알기 쉽도록
* 뚜렷한 반응성 - 행위에 의한 결과를 쉽게 인지

<br>
<br>

***


<br>
<br>

## 머티리얼 사용하기
> <a href="https://material.io/design/" target="_blank">공식 가이드</a>  

<br>

### 1. flat한 요소들을 입체적으로 사용
* block/inline-block 요소들과 배경색 활용
* shadow로 깊이감 표현

<br>

### 2. 사용자 행동에 대한 반응 보여주기
* `:hover` 등 활용 (과하지 않도록!)

<br>

### 3. 메인 색(테마 색)을 지정하여 활용하기
> <a href="https://material.io/design/color/the-color-system.html#tools-for-picking-colors" target="_blank">공식 사이트에서 가이드하는 색상 보기</a>  

<br>

### 4. 구글 사이트들, 안드로이드 공식 앱들 참고 
* Gmail, Youtube, Firebase, 
* <a href="https://themes.material-ui.com/" target="_blank">Material UI Themes 보기</a>  

<br>

### 5. 라이브러리 활용하기 
> <a href="https://getmdl.io/" target="_blank">Material Design Lite</a>  

<br>
<br>

## 다음 강좌
* [08. CSS Reset & 실습과제](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/03-css/08/README.md)