# 08. HTML 구획과 시맨틱 요소

## [예제]
> 앞으로의 강좌에서 최종적으로 만들 웹사이트의 프로토타입  

![시맨틱 태그 예제 결과](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/02-html/08/screenshots/portfolio.png)

* <a href="https://gitlab.com/junseol86/fastcampus-lecture-codes/-/blob/master/2020-spring/02-html/08/portfolio-lecture.html" target="_blank">포트폴리오 예제 실습 코드</a>
* <a href="https://gitlab.com/junseol86/fastcampus-lecture-codes/-/blob/master/2020-spring/02-html/08/portfolio-complete.html" target="_blank">포트폴리오 예제 완성본 코드</a>
* <a download="photo.png" href="https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/02-html/08/photo.png" target="_blank">사용된 이미지 파일 다운</a>

<br>

***

<br>

## 구획을 나누는 태그

### `<div>` : division
* **순수 컨테이너** - 스스로는 아무것도 표현하지 않음
* 다른 요소들을 묶거나, 문서의 구획을 나누는 데 사용  
* 전역(공통) 속성만 가짐
> * 유사한 `<span>` (inline 요소)와 달리 `블럭 요소`  

#### `[한계]` 코드로는 용도를 추론할 수 없는 단순 컨테이너

<br>

***

<br>

## **Semantics** - 의미가 부여된 태그
각 구획의 용도와 내용의 의미를 태그로써 알 수 있도록 함  
검색엔진 등이 사이트를 분석하는 데 도움을 줌
> `<div>` 를 대체해서 사용

<br>

### `<header>`
* 페이지의 **최상단**에 위치
* 일반적으로 제목, 로고, 검색창 등의 내용 등 포함
* 페이지의 소개 및 탐색에 도움을 줌

<br>

### `<footer>`
* 페이지의 **최하단**에 위치
* 일반적으로 작성자, 저작권 정보, 관련 문서 등 포함

<br>

### `<nav>`
* **링크들**(현재 페이지 내 구획 또른 다른 페이지로의)을 포함

<br>

### `<aside>`
* 문서의 주 내용과 간접적으로 연관된 부분
* 주로 사이드바 등에 사용

<br>

### `<main>`
* `<body>`의 **주요 컨텐츠**
* 메인 컨텐츠가 이 구획에 들어감

<br>

### `<section>`
* 컨텐츠 내 큰 단위의 **독립적인 구획**
* 다른 시맨틱 태그의 의미에 해당하지 않는 구획

<br>

### `<article>`
* 독립적으로 재사용되거나 반복적으로 나타나는 구획
* 게시판이나 뉴스, 갤러리의 목록상 각 항목 

<br>

***

<br>

## 다음 강좌
* [09. HTML 폼 사용하기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/02-html/09/README.md)