# 05. HTML 텍스트 기본 태그 2

## [예제]
![숭례문 페이지 태그 예제 결과](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/02-html/05/screenshots/gate.png)   

* <a href="https://gitlab.com/junseol86/fastcampus-lecture-codes/-/blob/master/2020-spring/02-html/05/gate-lecture.html" target="_blank">숭례문 예제 실습 코드</a>
* <a href="https://gitlab.com/junseol86/fastcampus-lecture-codes/-/blob/master/2020-spring/02-html/05/gate-complete.html" target="_blank">숭례문 예제 완성본 코드</a>
* <a download="photo.png" href="https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/02-html/05/sungryemoon.jpg" target="_blank">사용된 이미지 파일 다운</a>

<br>

***

<br>

## 기타 텍스트 관련 태그

`<mark>` : **사용자가 관심을 가질 만한** 정보 강조
* `예` 검색 등 사용자의 행동과 연관 있는 단어

`<sup>` : **superscript** (위첨자)
* 거급제곱, th 등 관례적으로 위 첨자로 넣는 글자일 때

`<sub>` : **subscript** (아래첨자)
* 화학 기호 등 관례적 아래 첨자

`<s>` : **strike** (취소선)

<br>

***

<br>

## 인용문 태그

### `<blockquote>` : 텍스트가 긴 인용문에 사용  
|속성|역할|값|
|:--|:--|:--|
|cite|인용문의 출처 URL|URL 텍스트|

<br>

***

<br>

## 가로선 태그

`<hr>` 태그: 주제나 분위기의 전환 구분
* 단순 밑줄은 CSS로 대신할 것

<br>

***

<br>

## 이미지 태그

### `<img>` : **image**

|속성|역할|값|
|:--|:--|:--|
|src|포함하고자 하는 이미지의 절대/상대 경로|경로 텍스트|
|alt|이미지 설명|설명문 텍스트|
|width|이미지의 가로 길이|단위 없는 정수값|

> * `alt` 속성 : 정보 용도
>   + 스크린 리더가 읽어주는 값
>   + 네트워크 오류, 컨텐츠 차단 죽은 링크 등의 상황 대처

> * `width는` CSS로 대체 가능

<br>

***

<br>

## 하이퍼링크 태그
다른 페이지나 같은 페이지의 어느 위치, 파일, 이메일 주소와 그 외 다른 URL로 연결할 수 있는 하이퍼링크 설정

### `<a>` : anchor

|속성|역할|값|
|:--|:--|:--|
|href|하이퍼링크가 가리키는 URL|`http:`/`https:` + 사이트 주소, `mailto:` + 메일주소, `tel:` + 전화번호|
|target|링크의 페이지를 열 창 지정|`_self` (현재 창), `_blank` (새 탭)...

<br>

***

<br>

## 다음 강좌
* [06. HTML 중첩되는 태그](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/02-html/06/README.md)